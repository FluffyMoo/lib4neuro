4Neuro v.0.1 - doplnit ASCII

## Dependencies
cmake (version>=3.0), make, Fortran compiler, Linux/Windows

## Installation

#### Windows
1. Clone the repository to your PC
```
git clone git@code.it4i.cz:moldyn/4Neuro.git
```

2. Go to the repository folder
3. Create folder 'build'
4. Go to the 'build' folder
5. Run `cmake` with following parameters
   - `MAKEFILE_TYPE` "MSYS Makefiles" for MSYS, "MinGW Makefiles" for MinGW
      and "Unix Makefiles" for Cygwin
   - `BUILD_TYPE` Release/Debug
   - `WITH_TIME_PROFILING` 0/1

    ```
    cmake -G MAKEFILE_TYPE -DCMAKE_BUILD_TYPE=BUILD_TYPE -DWITH_TIME_PROFILING:BOOLEAN=WITH_TIME_PROFILING ..  
    ```
   
6. Run `make`
7. Run `make install` (if you want to have 4Neuro in system paths)


#### Linux
1. Clone the repository to your PC
```
git clone git@code.it4i.cz:moldyn/4Neuro.git
```

2. Go to the repository folder
3. Set up variables in 'build.sh'
4. Run `./build.sh`

## Removing installation

#### Windows
1. Go to the repository folder
2. Delete 'build' folder

#### Linux
1. Go to the repository folder
2. Run `./clean.sh`
