#!/bin/bash

#------------#------------------------------------------------------------
# Parameters #
#------------#

# Fortran compiler
# FORTRAN_COMPILER=/mingw32/bin/gfortran
# FORTRAN_COMPILER=gfortran
# Build type (Release/Debug)
BUILD_TYPE=Debug

# Turn of/off time profiling (1/0)
WITH_TIME_PROFILING=0

#--------------#----------------------------------------------------------
# Library path #
#--------------#

MAKEFILE_TYPE=""
LIB_PATH="$PWD/build/lib"
#LIB_PATH_WIN="$CD\\build\\lib"

echo ${LIB_PATH}
#echo ${LIB_PATH_WIN}

case `uname -s` in

  Darwin)
    echo 'OS X not supported! Buy a normal computer, please.'
    exit -1
    ;;
    	
  Linux)
    MAKEFILE_TYPE="Unix Makefiles"
    if [[ ${LD_LIBRARY_PATH} != *${LIB_PATH}* ]]; then
      export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${LIB_PATH} 
    fi
    ;;
    
  CYGWIN*)
    MAKEFILE_TYPE="Unix Makefiles"
#    if [[ ${PATH} != *${LIB_PATH_WIN}* ]]; then
#      export PATH=${PATH};${LIB_PATH_WIN}
#    fi
    ;;
    
  MINGW*)
  	MAKEFILE_TYPE="MinGW Makefiles"
#  	 if [[ ${PATH} != *${LIB_PATH_WIN}* ]]; then
#      export PATH=${PATH};${LIB_PATH_WIN}
#    fi
    ;;
    
  MSYS*)
    MAKEFILE_TYPE="MSYS Makefiles"
    echo ${MAKEFILE_TYPE}
#    if [[ ${PATH} != *${LIB_PATH_WIN}* ]]; then
#      export PATH=${PATH};${LIB_PATH_WIN}
#    fi
    ;;
    
  *)
    echo "Your makefile generator `uname -s` is not yet supported. Please, write an issue to https://code.it4i.cz/moldyn/4Neuro" 
    exit -1
    ;;   
  	  
esac
#-------------------------------------------------------------------------

echo "Creating folder 'build'...";
mkdir -p build/lib;
echo "Folder 'build' was created'";

cd build;
#cmake -G "MSYS Makefiles" -DCMAKE_Fortran_COMPILER=${FORTRAN_COMPILER} -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DWITH_TIME_PROFILING:BOOLEAN=${WITH_TIME_PROFILING} ..
cmake -G "${MAKEFILE_TYPE}" -DCMAKE_BUILD_TYPE=${BUILD_TYPE} -DWITH_TIME_PROFILING:BOOLEAN=${WITH_TIME_PROFILING} ..
make VERBOSE=1 && echo "Build complete." || exit -1;
#make install;
