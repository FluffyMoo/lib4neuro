echo "Generating documentation into folder 'docs'...";
doxygen >/dev/null && echo "Documenation was generated." || exit -1;
cd docs/latex;
echo "Building LaTeX documentation../";
make >/dev/null && echo "Documentation was built." || exit -1;
cd ../..;
