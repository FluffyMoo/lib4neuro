#!/bin/bash

. compilers.env

export FSFLAG='-cpp -I'

echo "#------------------#----------------------------------------------------------------------------------"
echo "# FUnit testing... #"
echo "#------------------#"
FUNIT_SCRIPTS=connection_m
cd src;
for F in ${FUNIT_SCRIPTS}; do
    funit ${F};
done

echo "#---------------------#-------------------------------------------------------------------------------"
echo "# Memcheck testing... #"
echo "#---------------------#"
MEM_TEST_SCRIPTS=connection_m_mem_leak_test.out

cd ../build;

for F in ${MEM_TEST_SCRIPTS}; do
    echo "Running ${F}..."
    TEST1_MEM_STATUS=`valgrind --leak-check=yes build/${F} 2>&1 | grep 'LEAK SUMMARY' | wc -l`

    if [[ TEST1_MEM_STATUS -gt 0 ]]; then
        echo "ERROR: Memory leaks detected in ${F}!"
        exit -1
    fi
    echo "${F} OK"
done

echo "#---------------------------#"
echo "# No memory leaks detected. #"
echo "#---------------------------#"
