!> Unit test suite for connections (synapses)
!! in neural networks.
!!
!! @author Martin Beseda
!! @author Martin Mrovec
!! @date 2017
test_suite connection_m

! Global variables declaration
type(neuron_t), target :: n1
type(neuron_t), target :: n2

type(neuron_t), pointer :: n1_p
type(neuron_t), pointer :: n2_p

type(neuron_t), pointer :: dummy_p

type(connection_t), pointer :: con

!------------------------!---------------------------------------------------------------
! Setup before each test !
!------------------------!
setup
  write(*,*) '+------------------------+'
  write(*,*) '| SETUP BEFORE UNIT TEST |'
  write(*,*) '+------------------------+'
  write(*,*) 'Creating instances of the class neuron_t...'
  n1 = neuron_t(21.3)
  n2 = neuron_t(13.7)
  write(*,*) 'Assigning instances to pointer...'
  n1_p => n1
  n2_p => n2
  write(*,*) 'Creating an instance of the class interval_connection_t...'
  con => connection_t(n1_p, n2_p, 5.25)
  nullify(dummy_p)
  write(*,*) 'Ready for test.'
end setup

!-------------------------!--------------------------------------------------------------
! Cleanup after each test !
!-------------------------!
teardown
  write(*,*) '+-------------------------+'
  write(*,*) '| CLEANUP AFTER UNIT TEST |'
  write(*,*) '+-------------------------+'
  write(*,*) 'Deallocating everything.'
  deallocate(con)
  nullify(dummy_p)
  write(*,*) 'Cleaned succesfully.'
end teardown

!-------!--------------------------------------------------------------------------------
! Tests !
!-------!


!-----------------------------!
! Test of getters and setters !
!-----------------------------!
test getters_setters
  write(*,*) '+++ Test of getters and setters ...'
  ! Test of get_weight
  assert_real_equal(con%get_weight(), 5.25e+0)
  ! Test of adjust_weight
  call con%adjust_weight(1.0)
  assert_real_equal(con%get_weight(), 6.25e+0)
  write(*,*) '... finished +++'
end test

!----------------------------------!
! Test of input and output neurons !
!----------------------------------!
test input_and_output_neurons
  write(*,*) '+++ Test of input and output neurons ...'
  ! Input neuron
  dummy_p => con%get_input_neuron()
  assert_real_equal(dummy_p%get_state(),21.3d+0)
  ! Output neuron
  dummy_p => con%get_output_neuron()
  assert_real_equal(dummy_p%get_state(),13.7d+0)
  write(*,*) '... finished +++'
end test

!--------------------------!
! Test of passing a signal !
!--------------------------!
test pass_signal
   write(*,*) '+++ Test of passing a signal ...'
   dummy_p => con%get_output_neuron()
   call con%pass_signal()
   assert_real_equal(dummy_p%get_state(), 111.825d+0)
   write(*,*) '... finished +++'
end test


end test_suite
