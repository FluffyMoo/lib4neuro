program connection_mem_leak_test
    use connection_m
    use neuron_dummy_m
    use normal_m

    type(neuron_t), target :: n1
    type(neuron_t), target :: n2

    type(neuron_t), pointer :: n1_p
    type(neuron_t), pointer :: n2_p
    type(neuron_t), pointer :: dummy_p

    type(interval_connection_t), pointer :: con1, con2 

    print *, '+---------------------------------------------------------+'
    print *, '| STARTING MEMORY LEAK TESTING OF THE MODULE CONNECTION_M |'
    print *, '+---------------------------------------------------------+'

    print *, 'Creating instances of the class neuron_t...'
    n1 = neuron_t(21.3)
    n2 = neuron_t(13.7)
    print *, 'neuron_t instances were created successfully.'

    print *, 'Assigning instances to pointer...'
    n1_p => n1
    n2_p => n2
    print *, 'Assignment was successful.'

    print *, 'Creating an instance of the class interval_connection_t with 3-parameters constructor...'
    con1 => interval_connection_t(input_neuron=n1_p, &
                                 output_neuron=n2_p,&
                                 weight=5.25)

    print *, 'Creating an instance of the class interval_connection_t with 2-parameters constructor...'
    con2 => interval_connection_t(input_neuron=n1_p, output_neuron=n2_p)

    deallocate(con1)
    deallocate(con2)
end program connection_mem_leak_test
