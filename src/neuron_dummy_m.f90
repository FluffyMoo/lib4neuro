module neuron_dummy_m
    implicit none

    public

    ! TODO smazat
    !> neuron class
    type :: neuron_t
        real :: state

    contains
        procedure :: get_state => get_state_impl
        procedure :: set_state => set_state_impl
    end type neuron_t

    contains

        !--------------!
        ! class neuron_t !
        !--------------!
        ! TODO smazat
        function get_state_impl(this) result(state)
            class(neuron_t), intent(in) :: this
            real                      :: state

            state = this%state
        end function get_state_impl

        ! TODO smazat
        subroutine set_state_impl(this, new_state)
            class(neuron_t), target  :: this
            real, intent(in)       :: new_state

            this%state = new_state
        end subroutine set_state_impl



end module
