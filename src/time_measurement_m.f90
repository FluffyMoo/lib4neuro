!> Module containing function for time measurement, mostly
!! for the time profiling regime
!!
!! @author Martin Beseda
!! @date 2017
module time_measurement_m
    implicit none

    public

    contains

    !> Subroutine for starting time profiling
    !! @param[in,out] start_time Parameter, where current time will be saved (real number)
    subroutine time_profiling_start(start_time)
        real, intent(inout) :: start_time

        call cpu_time(start_time)
    end subroutine time_profiling_start

    !> Subroutine for stopping time profiling and
    !! printing the result.
    !!
    !! @param[in] start_time  Parameter containing starting time (real number)
    !! @param[in] region_name Name of the measured region (string)
    subroutine time_profiling_stop(start_time, region_name)
        real, intent(in)  :: start_time
        character(len=*) :: region_name 
        real              :: stop_time
        character(len=35) :: s        

        s = '| TIME PROFILING | Function '

        call cpu_time(stop_time)
        write(*, "(A)") '+----------------+'
        write(*, "(A,A,A,E15.7,A)") s,region_name,'() was running for ',stop_time-start_time,'s.'
        write(*, "(A)") '+----------------+'   
    end subroutine time_profiling_stop

end module time_measurement_m
